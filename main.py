import os, csv, time
from encryption import *
from compression import *

# Get the file name
file_in = raw_input('Enter the name of the file, or its file path: ')

# Start tracking how long the process takes
start_tracking = time.time()
compress('compressed.gz',file_in) # Compress file and store in compressed.gz
write_key()
key = load_key()
encrypt('compressed.gz', 'encrypted_compressed_file.gz', key) # Encrypt the zip file compressed.gz 
decrypt('encrypted_compressed_file.gz', 'decrypted_compressed_file.gz', key) #Decrypt the zip file 
decompress('decompressed.csv', 'decrypted_compressed_file.gz') # Decompress the file
end_tracking = time.time()
process = end_tracking - start_tracking
print('The process took %f seconds to execute.'%process)
