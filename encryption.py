# This code is built based on the AES algorithm
from cryptography.fernet import Fernet
from compression import *
import time

# Generates a key and saves it to a file
def write_key():
    key = Fernet.generate_key()
    with open("key.key", "wb") as key_file:
        key_file.write(key)

def load_key():
    return open('key.key','rb').read()

# Encrypts compressed file
def encrypt(compressed_file, send_to, key):
    f = Fernet(key) # Initialise the Fernet class

    with open(compressed_file,'rb') as file: # Open the file you wish to encrypt
        data = file.read()

    start_tracking = time.time()
    encrypted = f.encrypt(data)
    end_tracking = time.time()
    process = end_tracking - start_tracking
    with open(send_to,'wb') as file: # Write the encrypted data to a new file
        file.write(encrypted)
    
    print('Encryption took %f seconds to execute.'%process)
    
# Decrypts file
def decrypt(encrypted_file, send_to, key):
    f = Fernet(key) # Initialise the Fernet class

    with open(encrypted_file,'rb') as file: # Open the encrypted file
        encrypted = file.read()

    start_tracking = time.time()
    decrypted = f.decrypt(encrypted)
    end_tracking = time.time()
    process = end_tracking - start_tracking
    
    with open(send_to,'wb') as file:
        file.write(decrypted)

    print('Decryption took %f seconds to execute.'%process)
