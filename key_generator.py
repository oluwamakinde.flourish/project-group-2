from cryptography.fernet import Fernet

def generate(key_location):
    key = Fernet.generate_key()
    with open(key_location, "wb") as key_file:
        key_file.write(key)

def main():
    filename = input('Encryption key file name: ')
    generate(filename)
    print('This encrypted key is located in ' + filename)

if __name__ == '__main__':
    main()