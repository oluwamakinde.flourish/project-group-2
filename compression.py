import gzip
import shutil
import pandas as pd
import time

def compress(compressed_file, input_file):
    with open(input_file, 'rb') as f_in:

        start_tracking = time.time()
        with gzip.open(compressed_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        end_tracking = time.time()
        process = end_tracking - start_tracking
    print('Compression took %f seconds to execute.'%process)


# The decompress_function decompresses an "input_file" and outputs a decompressed file "decompressed_file"
def decompress(decompressed_file, input_file):
    with gzip.open(input_file, 'rb') as f_in:

        start_tracking = time.time()
        with open(decompressed_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        end_tracking = time.time()
        process = end_tracking - start_tracking
    print('Decompression took %f seconds to execute.'%process)

    #print(pd.read_csv(decompressed_file))  # reads the decompressed file

